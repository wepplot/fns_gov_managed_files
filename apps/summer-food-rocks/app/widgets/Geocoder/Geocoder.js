define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/_base/array',
    'dojo/query',
    'dojo/on',

    'dijit/_WidgetBase',

    'esri/dijit/Geocoder',
    'esri/geometry/Extent',
    'esri/InfoTemplate',
    'esri/symbols/PictureMarkerSymbol',
    'esri/layers/GraphicsLayer',
    'esri/graphicsUtils',


], function(
    declare, lang, arrayUtil, dojoQuery, on, _WidgetBase,
    GeoCoder, Extent, InfoTemplate, PMS, GraphicsLayer, GraphicsUtils) {

    return declare([_WidgetBase], {

        map: null,
        geocodeLayer: null,

        postCreate: function() {
            this.geocoder = new GeoCoder({
                map: this.map,
                autoComplete: false,
                minCharacters: 3,
                arcgisGeocoder: {
                    placeholder: 'Search City, State, or Zip',
                    sourceCountry: 'USA', // limit search to US
                    searchExtent: new Extent({
                        xmin: -170,
                        ymin: 16,
                        xmax: -62,
                        ymax: 71,
                        spatialReference: {wkid: 4326}
                    })
                }
            }, this.containerNode);

            // add a graphics layer for geocoding result.
            // Better than adding to map.graphics because clearing map.graphics
            // will clear selection symbols as well as geocoder results.
            if (!this.geocodeLayer) {
                this.geocodeLayer = new GraphicsLayer({
                    id: 'geocodeResults'
                });
                this.map.addLayer(this.geocodeLayer);
            }

            this.findSymbol = new PMS({
                'angle': 0,
                'xoffset': 0,
                'yoffset': 10,
                'type': 'esriPMS',
                'url': 'http://static.arcgis.com/images/Symbols/Basic/RedShinyPin.png',
                'contentType': 'image/png',
                'width': 24,
                'height': 24
            });

            this.domNode = this.geocoder.domNode;

            this.infoTemplate = new InfoTemplate(
                'Location',
                'Address: ${address}'
            );

        },

        startup: function() {
            var self = this;
            this.geocoder.startup();

            // when the X in the search box is clicked, clear the geocoder result graphics
            on(dojoQuery('.esriGeocoderReset')[0], 'click', function(evt) {
                self.geocodeLayer.clear();
            });

            on(this.geocoder, 'select', lang.hitch(this, this.handleResults));
            on(this.geocoder, 'keypress', lang.hitch(this, this.esriGeocoder_KeypressHandler));

        },

        esriGeocoder_KeypressHandler: function(e) {
            if (e.keyCode === 13) {
                e.preventDefault();
            }
        },
        
        handleResults: function(response) {
            var feat;
            var self = this;

            // if we don't have a feature, return
            if (!response || !response.result || !(feat = response.result.feature)) {
                return;
            }

            // clear old geocode results
            this.geocodeLayer.clear();

            feat.setSymbol(this.findSymbol);
            var featAddress = response.result.name || null;
            if (featAddress) {
                feat.attributes.address = featAddress;
                feat.setInfoTemplate(this.infoTemplate);
            }
            this.geocodeLayer.add(feat);

            // if the resulting zoom level is closer than 12, zoom back out to 12.
            // we're really hijacking the map mid-extent-change, so this will throw some errors.
            // the alternative is to wait for the extent to fully change (deferred), *then* zoom out.
            on.once(this.map, 'extent-change', function() {
                if (self.map.getZoom() > 12 && feat.geometry) {
                    self.map.centerAndZoom(feat.geometry, 12);
                }
            });
        }

    });
});