require([
    'esri/arcgis/utils',
    'esri/layers/GraphicsLayer',
    'app/widgets/Geocoder/Geocoder',

    'esri/dijit/Scalebar',
    'esri/dijit/HomeButton',
    'esri/dijit/LocateButton',
    'esri/dijit/PopupMobile',

    'dojo/on',
    'dojo/dom',
    'dojo/dom-style',
    'dojo/dom-construct',
    'dojo/_base/array',
    'dojo/query',
    'dijit/Dialog',

    'dojo/text!app/templates/modalText.html',
    'dojo/text!app/templates/popupTemplate.html',

	'dojo/domReady!'],
function(
    arcgisUtils, GraphicsLayer, Geocoder,
    Scalebar, HomeButton, LocateButton, PopupMobile,
    dojoOn, dom, domStyle, domConstruct, arrayUtil, query, Dialog,
    modalText, replacementInfotemplate) {

    // some variables to customize:
    var widthBreakpoint = 540; // the break between regular and mobile popups on non touch screens
    var resizeTimeoutInterval = 100; // wait this many milliseconds to respond to window resize event
    var hungerLayerPrefix = 'WhyHunger_OpenSites_20';

    // construct and the modal instructions window
    var mapWidth = domStyle.get(dom.byId('map'), 'width');
    // we don't actually need this default 'smallMap' class,
    // but ie8 freaks out if baseClass in the dialog constructor is null.
    var dialogClass = 'smallMap';
    if (mapWidth > 1000) {
        dialogClass = 'wideMap';
    } else if (mapWidth > 700) {
        dialogClass = 'mediumMap';
    }

    var instructionsModal = new Dialog({
        title: 'Find Summer Food Sites',
        draggable: false,
        baseClass: dialogClass,
        id: 'instrModal', // use id to more easily override dijit css
        content: modalText // imported with dojo/text in the require statement
    });
    instructionsModal.show();

    // decide whether or not to use mobile popups.
    // because of limitations of the regular infowindow, this app will always
    // use mobile popups on any device with touch events.
    var isTouch = 'ontouchstart' in window || (navigator.maxTouchPoints > 1) || (navigator.msMaxTouchPoints > 1);
    var useMobilePopup = isTouch || document.body.clientWidth < widthBreakpoint;

    arcgisUtils.createMap('6505bf11102e4f5dbf7955bd1ed02050', 'map').then(function(response) {
        var map = response.map;

        // this graphics layer is necessary to create here, instead of in the geocoder widget,
        // because we have TWO geocoders, and they need to use the same results layer.
        var geocodeLayer = new GraphicsLayer({
            id: 'geocodeResults'
        });
        map.addLayer(geocodeLayer);

        createLotsOfWidgets(map, geocodeLayer);

        // store regular popup and create/store mobile popup for future use.
        var regularPopup = map.infoWindow;
        var mobilePopup = new PopupMobile(null, domConstruct.create('div'));

        if (useMobilePopup) {
            map.setInfoWindow(mobilePopup);
        }

        var hungerSitesLayer = null;
        // use .some instead of .forEach to take advantage of its short-circuit.
        arrayUtil.some(map.graphicsLayerIds, function(lyrId) {
            if (lyrId.indexOf(hungerLayerPrefix) === 0) {
                hungerSitesLayer = map.getLayer(lyrId);
                return true;
            }
            return false;
        });
        if (!hungerSitesLayer) {
            console.warn('No layer found with id starting with: ' + hungerLayerPrefix + '.');
            return;
        }

        overrideInfotemplate(hungerSitesLayer);

        // on window resize, we decide whether or not to change the popups based on the document's width.
        // because we always use mobile popups on touch devices, only add this listener
        // if this isn't a touch device.
        // this happens last because it isn't as important as the other functionality here, and if it
        // fails for some reason, everything else will have happened by now.
        if (!isTouch) {
            var resizeTimer;
            if (window.addEventListener) {
                window.addEventListener('resize', function(evt) {
                    clearTimeout(resizeTimer);
                    resizeTimer = setTimeout(function() {
                        onWindowResize(map, regularPopup, mobilePopup);
                    }, resizeTimeoutInterval);
                });
            } else {
                window.attachEvent('onresize', function(evt) {
                    clearTimeout(resizeTimer);
                    resizeTimer = setTimeout(function() {
                        onWindowResize(map, regularPopup, mobilePopup);
                    }, resizeTimeoutInterval);
                });
            }
        }

        //replace ESRI logo with one that is not a hyperlink to esri.com
        domConstruct.destroy(query('.logo-med')[0]);
        domConstruct.create('div', {
            'class': 'logo-replacement'
        }, query('.esriControlsBR')[0]);

    }); // end of createMap callback


    function createLotsOfWidgets(thisMap, geoLayer) {
        // create and start the modal geocoder
        var modalGeocoder = new Geocoder({
            map: thisMap,
            containerNode: 'modalGeocoderDiv',
            geocodeLayer: geoLayer
        });
        dojoOn.once(modalGeocoder.geocoder, 'load', function() {
            instructionsModal.resize();
        });
        modalGeocoder.startup();

        // if the modal geocoder is used to find a location, we need to hide the modal window
        dojoOn.once(modalGeocoder.geocoder, 'select', function() {
            instructionsModal.hide();
            //$('#instructionsModal').modal('hide');
        });

        // create and start the main geocoder
        var geocoder = new Geocoder({
            map: thisMap,
            containerNode: 'geocoderDiv',
            geocodeLayer: geoLayer
        });
        geocoder.startup();

        // create the scalebar. doesn't need a startup.
        var scalebar = new Scalebar({
            map: thisMap,
            scalebarUnit: 'english'
        });

        // create and start the home button
        var homeButton = new HomeButton({
            map: thisMap,
            baseClass: 'map-button',
            extent: thisMap.extent // the docs say this is optional, but maybe not with createMap ?
        }, 'homeDiv');
        homeButton.startup();

        // create and start the locate button
        // (commented out because of privacy concerns)
        /*var locateButton = new LocateButton({
            map: thisMap,
            scale: 144447,
            baseClass: 'map-button'
        }, 'locatorDiv');
        locateButton.startup();*/
    }

    function onWindowResize(thisMap, regPopup, mobPopup) {
        var newWidth = document.body.clientWidth;

        // two cases for changing infowindows:
        // 1. large width -> small width
        // 2. small width -> large width

        if (newWidth < widthBreakpoint && thisMap.infoWindow === regPopup) {
            thisMap.infoWindow.hide();
            thisMap.setInfoWindow(mobPopup);
        } else if (newWidth >= widthBreakpoint && thisMap.infoWindow === mobPopup) {
            thisMap.infoWindow.hide();
            thisMap.setInfoWindow(regPopup);
        }
    }

    function overrideInfotemplate(lyr) {
        lyr.infoTemplate.setContent(replacementInfotemplate);
    }

});