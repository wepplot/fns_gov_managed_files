module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        // list the details of your tasks here
        watch: {
            thisNameDoesntMatter: {
                files: ['**/*', '!node_modules/**'], // everything! (except node modules)
                options: { livereload: true }
            },
            css: {
                files: ['**/*.css', '!app/resources/main.min.css'],
                tasks: ['cssmin'],
                options: { livereload: false }
            }
        },
        cssmin: {
            main: {
                src: 'dev/main.css',
                dest: 'app/resources/main.min.css'
            }
        }
    });

    // Load the plugins that provide the tasks.
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // Default task(s).
    grunt.registerTask('default', ['watch']);

};